

#PROG = usbasp
PROG = arduino
PORT = 
#PROG = avrisp2
BOARD = mini328
#BOARD = mega2560
#PORT = 
#PORT = -P /dev/ttyACM0
PORT = -P /dev/ttyUSB0 -b57600 -D




all:
	ino build
	avr-size .build/mini328/firmware.elf
	avr-objdump -h --source --demangle --wide --syms \
	.build/mini328/firmware.elf >.build/mini328/firmware.lss

clean:
	ino clean

upload:	upload_noee

upload_noee:
	avrdude -p atmega328p -c $(PROG) $(PORT) -v \
	 -U flash:w:.build/mini328/firmware.hex

upload_ee:
	avr-objcopy -j .eeprom --set-section-flags=.eeprom="alloc,load" \
         --change-section-lma .eeprom=0 -O ihex .build/mini328/firmware.elf \
	  .build/mini328/firmware.eep
	avrdude -p atmega328p -c $(PROG) -v \
	 -U flash:w:.build/mini328/firmware.hex \
	 -U eeprom:w:.build/mini328/firmware.eep

size:
	avr-size .build/mini328/firmware.elf



