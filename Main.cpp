/*
 * 
 * 
 * Serial protocol
 * all codes over 0xf0 needs to be escaped
 * 0xF0 is transmitted as 0xF0 0x00
 * 0xF1 is transmitted as 0xF0 0x01
 * 
 * Start of packet is 0xF1
 * 
 *  
*/

#include <Arduino.h>


#define HARDWARE_M 1
#include "hardware.h"

#include <avr/sleep.h>

#include "HardwareSerial.h"
#include <wiring_private.h>



extern HardwareSerial Serial;


uint8_t IsADCready(void) {
    return(bit_is_clear(ADCSRA, ADSC));
}

int16_t ReadB1voltage(void) {
    int16_t a;
    a=ADCW;
    B1_vtotal -= B1_voltage[ADC_counter];
    B1_voltage[ADC_counter]=a;
    B1_vtotal += a;
}

int16_t ReadB2voltage(void) {
    int16_t a;
    a=ADCW;
    B2_vtotal -= B2_voltage[ADC_counter];
    B2_voltage[ADC_counter]=a;
    B2_vtotal += a;
}

int16_t ReadB3voltage(void) {
    int16_t a;
    a=ADCW;
    B3_vtotal -= B3_voltage[ADC_counter];
    B3_voltage[ADC_counter]=a;
    B3_vtotal += a;
}

int16_t ReadBcurrentOut(void) {
    int16_t a;
    a=ADCW;
    B_ctotal -= B_current[ADC_counter];
    B_current[ADC_counter]=a;
    B_ctotal += a;
}

void SetOpAmpOn(void) {
    digitalWrite(OPAMP_PWR, HIGH);
}

void SetOpAmpOff(void) {
    digitalWrite(OPAMP_PWR, LOW);
}

// Private variables
//
uint8_t     ADC_ready_counter;

void setup() {
    uint8_t i;
    Serial.begin(9600);
    //
    pinMode(EXT_VOLTAGE_SENSE, INPUT);
    digitalWrite(EXT_VOLTAGE_SENSE, LOW);
    //
    //
    analogReference(DEFAULT);
    //
    //
    pinMode(BAT1_VI, INPUT);
    digitalWrite(BAT1_VI, LOW);
    pinMode(BAT2_VI, INPUT);
    digitalWrite(BAT2_VI, LOW);
    pinMode(BAT3_VI, INPUT);
    digitalWrite(BAT3_VI, LOW);
    pinMode(CURRENT_OUT, INPUT);
    digitalWrite(CURRENT_OUT, LOW);
    //
    // All pwms off
    pinMode(BAT1_PWM, OUTPUT);
    analogWrite(BAT1_PWM, C_PWM_OFF);
    pinMode(BAT2_PWM, OUTPUT);
    analogWrite(BAT2_PWM, C_PWM_OFF);
    pinMode(BAT3_PWM, OUTPUT);
    analogWrite(BAT3_PWM, C_PWM_OFF);
    pinMode(CHG_PWM, OUTPUT);
    analogWrite(CHG_PWM, C_PWM_OFF);
    //
    //
    pinMode(OPAMP_PWR, OUTPUT);
    SetOpAmpOn();
    //
    B1_vtotal=B2_vtotal=B3_vtotal=B_ctotal=0;
    ADC_counter=ADC_state=0;
    VoltageB1=VoltageB2=VoltageB3=CurrentB=0.0;
    flags1.val=0;
    memset(&B1_voltage,0,sizeof(B1_voltage));
    memset(&B2_voltage,0,sizeof(B2_voltage));
    memset(&B3_voltage,0,sizeof(B3_voltage));
    memset(&B_current,0,sizeof(B_current));
    ADC_ready_counter=0;
    SerialState=0;
    MyAddress=1;
}

uint32_t    ADC_delay;

void ADC_statemachine(void) {
    switch (ADC_state) {
        case 0:
            // power up opamp
            SetOpAmpOn();
            ADC_delay=millis()+50;
            ADMUX = (ADMUX & 0xf8); // A0
            break;
        case 1:
            if (ADC_delay>=millis()) {
                ADC_state--;
            }
            break;
        case 2:
            if (IsADCready()) {
                ReadB1voltage();
                ADMUX = (ADMUX & 0xf8) | 0x01; // A1
            } else {
                ADC_state--;
            }
            break;
        case 3:
            if (IsADCready()) {
                ReadB2voltage();
                ADMUX = (ADMUX & 0xf8) | 0x02; // A2
            } else {
                ADC_state--;
            }
            break;
        case 4:
            if (IsADCready()) {
                ReadB3voltage();
                ADMUX = (ADMUX & 0xf8) | 0x03; // A3
            } else {
                ADC_state--;
            }
            break;
        case 5:
            if (IsADCready()) {
                ReadBcurrentOut();
            } else {
                ADC_state--;
            }
            break;
        default:
            ADC_state=0xff;
            F_ADCfinish=true;
            if (++ADC_ready_counter==C_MAX_ADC_AVG) {
                F_ADCvalid=true;
            }
            //
            // power down the adc hardware
            SetOpAmpOff();
            //
            break;
    }
    ADC_state++;
}

uint8_t     CharCookedRead;

void CookSerial() {
    uint8_t     a;
    a=Serial.read();
    if (F_cookinternalflag) {
        CharCookedRead=0xf0 | a;
        F_SerialAvailable=true;
    } else {
        F_cookinternalflag = (a==0xf0);
        if (!F_cookinternalflag) {
            CharCookedRead=a;
            F_SerialAvailable=true;
        }
    }
}

void processSerial(void) {
    switch (SerialState) {
        case 0:
            if (CharCookedRead == C_485_START_OF_TX) {
                SerialState++;
            }
            break;
        case 1:
            if (CharCookedRead == MyAddress) {
                SerialState++;
            } else {
                SerialState=0;
            }
            break;
        case 2:
            //
            // fill up the command buffer
            break;
        case 3:
            //
            // do checksum on command buffer
            break;
        default:
            SerialState=0;
            break;
    }
}

void loop() {
    if (!F_ADCfinish) {
        ADC_statemachine();
        if (F_ADCfinish && F_ADCvalid) {
            // now do the magic
            //
            
        }
    }
    if (Serial.available()) {
        CookSerial();
        if (F_SerialAvailable) {
            processSerial();
            F_SerialAvailable=false;
        }
    }
}



