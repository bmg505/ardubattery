/* 
 * File:   hardware.h
 * Author: leon
 *
 * Created on August 2017
 */

#ifndef HARDWARE_H
#define	HARDWARE_H

#include <stdint.h>


//#include "EEPROM.h"

#define SMD_CPU 1



#define DEBUG_PRINT_SIZE_OF_STRUCTURE 0
#define DEBUG_PRINT_SIZE_OF_EECONFIG 0

#define ENABLE_WDT 0


#define SER_DEBUG 0


#define C_MAX_ADC_AVG 10

#define C_485_ESCAPE_CHAR 0xF0
#define C_485_START_OF_TX 0xF1

#undef AA
#if HARDWARE_M
#define AA
#else
#define AA extern
#endif

#define FS(x) (__FlashStringHelper*)(x)

#define FS_PSTR(x) FS(PSTR(x))


#define BAT1_VI A0
#define BAT2_VI A1
#define BAT3_VI A2
#define CURRENT_OUT A3
#if SMD_CPU
#define CURRENT_CHG A7
#else
#define CURRENT_CHG A4
#endif

#define CHG_PWM 3
#define BAT1_PWM 9
#define BAT2_PWM 10
#define BAT3_PWM 11
#define OPAMP_PWR 12





#define EXT_VOLTAGE_SENSE 8
#define C_PWM_OFF 0





AA uint32_t ms_counter;

typedef union {
    uint8_t     val;
    struct {
        uint8_t         b0:1;
        uint8_t         b1:1;
        uint8_t         b2:1;
        uint8_t         b3:1;
        uint8_t         b4:1;
        uint8_t         b5:1;
        uint8_t         b6:1;
        uint8_t         b7:1;
    };
} Bits;



AA int16_t      B1_voltage[C_MAX_ADC_AVG], B2_voltage[C_MAX_ADC_AVG];
AA int16_t      B3_voltage[C_MAX_ADC_AVG], B_current[C_MAX_ADC_AVG];

AA int16_t      B1_vtotal,B2_vtotal,B3_vtotal,B_ctotal;
AA uint8_t      ADC_counter,ADC_state;
AA uint8_t      SerialState,MyAddress;

AA float        VoltageB1,VoltageB2,VoltageB3,CurrentB;
AA Bits         flags1;


//
// flag is set when a whole scan of the adc subsystem is finished
//
#define F_ADCfinish flags1.b0

//
//
// This flag gets set when the battery is at a critical level
#define F_BATcritical flags1.b1

//
// Set when ADC values read becomes valid
#define F_ADCvalid flags1.b2

//
// used internally to the cook routine
// Cook the serial  interface
#define F_cookinternalflag flags1.b3

//
// Sets is a serial char is available
#define F_SerialAvailable flags1.b4

//

//#define  flags1.b5

//
//

#endif	/* HARDWARE_H */

